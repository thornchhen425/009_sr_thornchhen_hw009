import React, { useState } from "react";
import Menu from "./components/Menu";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import Home from "./pages/Home";
import Video from "./pages/Video";
import Account from "./pages/Account";
import Welcome from "./pages/Welcome";
import Auth from "./pages/Auth";
import Detail from "./pages/Detail";
import Movie from "./pages/Movie";
import Animation from "./pages/Animation";

export default function App() {

  const [card, setCard] = useState(
    [
      {
        image: "/images/01.jpg",
        id: 1,
        title: "Seven Deadly Sins",
        description: "Content",
      },
      {
        image: "/images/02.jpeg",
        id: 2,
        title: "Demon Slayer ",
        description: "Content",
      },
      {
        image: "/images/03.jpg",
        id: 3,
        title: "Sword Art Online",
        description: "Content",
      },
      {
        image: "/images/01.jpg",
        id: 4,
        title: "Seven Deadly Sins",
        description: "Content",
      },
      {
        image: "/images/05.jpeg",
        id: 5,
        title: "Jujutsu Kaisen",
        description: "Content",
      },
      {
        image: "/images/06.jpg",
        id: 6,
        title: "One Punch",
        description: "Content",
      },
    ]
  )

  const [isSignin, setSignin] = useState(false);

  function onSignin() {
    setSignin(!isSignin);
  }

  return (
    <Router>
      <Route render={() => <Menu />} />
      <Container>
        <Switch>
          <Route exact path="/" render={()=> <Home card={card}/>}/>
          <Route path="/detail/:id" render={()=> <Detail/>}/>
          <Route path="/video/movie" render={()=> <Movie/>}/>
          <Route path="/video/animation" render={()=> <Animation/>}/>
          <Route path="/video" component={Video} />
          <Route path="/account" component={Account} />
          <Route path="/welcome" render={()=> <Welcome />}/>
          <Route path="/auth" render={() => <Auth onSignin={onSignin} isSignin={isSignin}/>}/>
          <Route path="*" render={()=> <h1>Page Not Found</h1>}/>
        </Switch>
      </Container>
    </Router>
  );
}
