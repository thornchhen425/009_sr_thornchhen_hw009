import React from "react";
import { ButtonGroup, Button } from "react-bootstrap";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Movie from "./Movie";
import { Link } from "react-router-dom";
import Animation from "./Animation";
import Switch from "react-bootstrap/esm/Switch";

export default function Video() {
  return (
    <div>
      <h1>Video</h1>
      <Router>
        <ButtonGroup aria-label="Basic example">
          <Button as={Link} to={"/video/movie"} variant="secondary">
            Movie
          </Button>
          <Button as={Link} to={"/video/animation"} variant="secondary">
            Animation
          </Button>
        </ButtonGroup>
        <Switch>
          <Route path="/video/movie" render={() => <Movie />} />
          <Route path="/video/animation" render={() => <Animation />} />
        </Switch>
      </Router>
    </div>
  );
}
