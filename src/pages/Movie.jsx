import React, { useState } from "react";
import {ButtonGroup, Button} from 'react-bootstrap'

export default function Movie() {
  const [name, setName] = useState("Advanture");
  return (
    <div>
      <h1>Movie Category</h1>
      <ButtonGroup aria-label="Basic example">
        <Button variant="secondary">Advanture</Button>
        <Button variant="secondary">Crime</Button>
        <Button variant="secondary">Action</Button>
        <Button variant="secondary">Romance</Button>
        <Button variant="secondary">Comedy</Button>
      </ButtonGroup>
      <h1>Please Choose Category: {name}</h1>
    </div>
  );
}
