import React, { Component } from "react";
import { Redirect } from "react-router";

export default function ProtectedRouter({
  isSignin,
  component: Component,
  ...rest
}) {
  if (isSignin) {
    return <Component {...rest} />;
  } else {
    return <Redirect to="/welcome" />;
  }
  return <div></div>;
}
