import React, { useState } from "react";
import { Nav, Card, Button, Row, Col } from "react-bootstrap";
import { Link, useHistory, useParams } from "react-router-dom";

export default function Home(props) {
  return (
    <Row>
      {
      props.card.map((item, index) => (
        <Col md={3} key={index} style={{margin:'20px 0'}}>
          <Card>
            <Card.Img variant="top" src={item.image}/>
            <Card.Body>
              <Card.Title>{item.title}</Card.Title>
              <Card.Text>{item.description}</Card.Text>
              <Button as={Link} to={`/detail/${item.id}`} variant="primary">
                Read
              </Button>
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  );
}
