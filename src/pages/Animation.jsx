import React from "react";
import { ButtonGroup,Button } from "react-bootstrap";

export default function Animation() {
  return (
    <div>
      <h1>Animation Category</h1>
      <ButtonGroup aria-label="Basic example">
        <Button variant="secondary">Action</Button>
        <Button variant="secondary">Romance</Button>
        <Button variant="secondary">Comedy</Button>
      </ButtonGroup>
      <h1>Please Choose Category: </h1>
    </div>
  );
}
