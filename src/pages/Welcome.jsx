import React from "react";
import {Button} from 'react-bootstrap'
import { Link } from "react-router-dom";

export default function Welcome(props) {
  return (
    <div>
      <h1>Welcome</h1>
      <Button as={Link}to="/auth" variant="primary">Log out</Button>
    </div>
  );
}
